/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rattanalak.car;

/**
 *
 * @author Rattanalak
 */
public class Car {

    private int x;
    private int y;
    private int fx;
    private int fy;
    private String color;
    private int speed;
    private int fuel;
    private char direc = 'N';
    private int km;

    public Car(int x, int y, String color, int speed, int fuel, int fx, int fy) {
        this.x = x;
        this.y = y;
        this.fx = fx;
        this.fy = fy;
        this.color = color;
        this.speed = speed;
        this.fuel = fuel;
    }

    public void setSpeed(int speed) {
        if (speed < 0) {
            System.out.println("Error : speed must higher than 0 !!");
            return;
        } else if (speed > 200) {
            System.out.println("Error : Speed should not higher than 200!!");
            return;
        }
        runOutFuel();
        this.speed = speed;
    }

    public boolean runCar(char direc) {
        switch (direc) {
            case 'N':
                if (runOutFuel()) {
                    return false;
                }
                reduceFuel();
                y = y + 1;
                break;
            case 'S':
                if (runOutFuel()) {
                    return false;
                }
                reduceFuel();
                y = y - 1;
                break;
            case 'E':
                if (runOutFuel()) {
                    return false;
                }
                reduceFuel();
                x = x - 1;
                break;
           case 'W':
                if (runOutFuel()) {
                    return false;
                }
                reduceFuel();
                 x = x + 1;
                break;
        }
        reFuel();
        return true;
    }

    private void reFuel() {
        if (x == fx && y == fy) {
            this.fuel = 100;
            System.out.println("Refuel!!");

        }
    }

    public boolean runCar(char direc, int km) {
        for (int i = 0; i < km; i++) {
            if (!this.runCar(direc)) {
                return false;
            }
        }
        return true;
    }

    private boolean reduceFuel() {
        if (speed <= 120) {
            fuel--;
        } else if (speed < 200) {

            fuel = fuel - 2;
        } else if (speed == 200) {

            fuel = fuel - 3;
        }
        return true;
    }

    private boolean runOutFuel() {
        if (fuel <= 0) {
            this.fuel = 0;
            this.speed = 0;
            System.out.println("Can't move : run out of fuel!");
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        System.out.println("Gas station (" + this.fx + ", " + this.fy + ")");
        return (this.color + " car (" + this.x + ", " + this.y
                + ")" + this.fuel + " is driving with speed =" + this.speed + " km/hr");
    }
}
